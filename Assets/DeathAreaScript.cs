﻿using UnityEngine;
using System.Collections;

public class DeathAreaScript : MonoBehaviour {

	//tuhotaan objekti kun se tulee triggerin sisään

	void OnTriggerEnter (Collider other)
	 {
		Destroy(other.gameObject);
	}
}
