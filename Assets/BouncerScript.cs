﻿using UnityEngine;
using System.Collections;

public class BouncerScript : MonoBehaviour {

	public float pushFactor = 500;

	void OnTriggerEnter(Collider other) 
	{
		if (other.tag == "Ball") 
		{
			Debug.Log("Kimmotan");
			Vector3 pushVector = other.transform.position - gameObject.transform.position;
			pushVector.Normalize();
			other.rigidbody.AddForce(pushVector*pushFactor);

		}
	}
}
