﻿using UnityEngine;
using System.Collections;

public class KameraScripti : MonoBehaviour
{
	
	public Transform target;
	public Transform ballPrefab;
	public Transform spawnPoint;
	
	
	void Update()
	{
		if (!target)
		{
			Instantiate(ballPrefab, spawnPoint.position, Quaternion.identity);
			var go = GameObject.FindWithTag("Ball");
			if (go)
				target = go.transform;
		}
		
		if (target)
			transform.position = new Vector3(target.position.x, target.position.y + 4, target.position.z -3);
	}
}